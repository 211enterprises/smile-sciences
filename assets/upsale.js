(function() {
  
  function UpSale() {}
  
  UpSale.prototype.getProduct = function(handle, callback) {
    Shopify.getProduct(handle, function(response) {
      if(typeof callback === 'function') {
        callback(response);
      }
    });  
  };
  
  UpSale.prototype.makeOffer = function(target, timing) {
    var timing = timing || 300;
    
    $(target).fadeIn(timing);
  };
  
  UpSale.prototype.setName = function(name, target) {
    $(target).append(name);
  };
  
  UpSale.prototype.setThumbnail = function(thumbnail, target) {
    $(target).append('<img src="' + thumbnail + '" />');
  };
  
  UpSale.prototype.setDescription = function(desc, target) {
    $(target).append(desc);
  };
  
  UpSale.prototype.setPrice = function(price, target) {
    $(target).append(Shopify.formatMoney(price));
  };
  
  UpSale.prototype.checkProductInCart = function(item, callback) {
    var item = item || {};
    
    Shopify.getCart(function(cart) {
      var cartItems = cart.items,
          variants = item.variants,
          count = 0;
    
      // Loop through items and make sure
      // current offer is not in cart      
      // If cart is not emtpy
      if(cartItems.length > 0) {
        for(c = 0; c < cartItems.length; c++) {
          for(i = 0; i < variants.length; i++) {
            if(cartItems[c].id === variants[i].id) {
              count++;
            }
          }
        }
      }
      
      // If count is 0, success, and return callback function
      if(count === 0 && cartItems.length > 0) {
        callback();
      }
    });
  };
  
  UpSale.prototype.acceptOffer = function(id, num, callback) {
    var num = num || 1;
    Shopify.addItem(id, num, function(response) {
      if(typeof callback === 'function') {
        callback(response);
      }
    });
  };
  
  UpSale.prototype.closeOffer = function(time) {
    var time = time || 300;
    
    $('.upsale-container').fadeOut(time)
    // Set cookie to not show offer twice
    document.cookie = "displayOffer=false";
  };

  // Instantiate upsale offer
  var upsale = new UpSale();
  
  upsale.getProduct('smile-sciences-teeth-whitening-pen', function(response) {
    // Check if cookie is set and not display offer in current session
    if(document.cookie.indexOf('displayOffer') < 0) {
      // Check if product is in cart
      // If so, do not send offer
      upsale.checkProductInCart(response, function() {
        // Show offer
        upsale.makeOffer('.upsale-container');
        
        upsale.setName(response.title, '#product-name');
        upsale.setThumbnail(response.images[0], '#product-thumbnail');
        upsale.setDescription(response.description, '#product-description');
        upsale.setPrice(response.price, '#product-price span');
        upsale.setPrice(response.price, '#product-price-large');
        
        // Decline offer
        $('button#no-thanks').bind('click', function() {
          upsale.closeOffer();
        });
        
        // Accept Offer
        $('button#add-to-cart').bind('click', function() {
          upsale.acceptOffer(response.variants[0].id, 1, function() {
            // Reload cart after successful offer
            location.reload();
          });
        });
      });
    }
  });

})(this);